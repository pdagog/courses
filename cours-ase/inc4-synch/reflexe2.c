// avant
if (x < 0)		// x est une variable partagée par plusieurs threads
    y = x ;		// x a pu être modifiée entre le test et l'affectation

// après
pthread_mutex_lock (&verrou) ;
if (x < 0)		// les modifications de x doivent elles aussi...
    y = x ;		// ... être réalisées en exclusion mutuelle
pthread_mutex_unlock (&verrou) ;
