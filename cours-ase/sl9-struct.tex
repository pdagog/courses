\incdir {inc9-struct}

\titreA {Structures de systèmes d'exploitation}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Introduction
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Introduction}

\begin {frame} {Introduction}
    Rappel (cf. chapitre introductif) : 

    \begin {quote}
	Le noyau est constitué de l'ensemble minimum des primitives
	système nécessaires pour atteindre les deux objectifs
	fondamentaux

    \end {quote}

    \bigskip

    \begin {minipage} {.5\linewidth}
	\fig {arch-linux} {}
    \end {minipage}%
    \begin {minipage} {.5\linewidth}
	Schéma d'un système \frquote{classique}
	\begin {itemize}
	    \item les primitives système sont celles de l'Unix
		traditionnel

	    \item le noyau gère toutes les ressources partagées

	\end {itemize}
    \end {minipage}

    \bigskip

    D'autres approches sont possibles : les primitives système ne
    correspondent pas forcément à la définition POSIX\footnote{POSIX
    définit une API, mais ne définit pas quelles fonctions sont des
    primitives et quelles fonctions sont des fonctions de bibliothèque.}.

\end {frame}

\begin {frame} {Introduction}
    Approches possibles
    \begin {itemize}
	\item noyau monolithique (la plupart des systèmes traditionnels)
	\item extension de noyau
	\item micronoyau
	\item exokernel
	\item unikernel
	\item etc.
    \end {itemize}
\end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Noyau monolithique
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Noyau monolithique}

\begin {frame} {Noyau monolithique}
    Les primitives système sont le mécanisme d'accès d'un processus
    au mode privilégié

    \bigskip

    \begin {minipage} {.35\linewidth}
	\fig {acces} {}
    \end {minipage}%
    \begin {minipage} {.65\linewidth}
	\begin {center}
	Inflation du nombre de primitives :
	\end {center}
	\ctableau {\fD} {|lccc|} {
	    \textbf{Système}
		& \textbf{Version}
		& \textbf{Année}
		& \textbf{Nb primitives} \\
	    \hline
	    Unix & v6 & 1975 & 43 \\
	    FreeBSD & 13 & 2019 & 541 \\
	    Linux & 5.2.5 & 2019 & 383 \\
	}
    \end {minipage}

    \bigskip

    Le noyau devient de plus en plus gros :
    \begin {itemize}
	\item d'environ \numprint{10000} lignes en 1975...
	\item ... à environ 20 millions de lignes en 2019 !
    \end {itemize}
\end {frame}

\begin {frame} {Noyau monolithique}
    Pourquoi cette inflation entre 1975 et maintenant ?

    \begin {itemize}
	\item optimisations
	\item nouveaux besoins 
	    \begin {itemize}
		\item réseau
		\item sécurité
		\item ...
	    \end {itemize}
	\item besoins plus sophistiqués
	    \begin {itemize}
		\item fiabilité des signaux v7 \implique signaux POSIX
		\item systèmes de fichiers plus robustes
		\item plusieurs systèmes de priorité des processus \\
		    (round-robin, temps réel, etc.)
		\item ...
	    \end {itemize}
	\item réalité diverse
	    \begin {itemize}
		\item gammes de matériels étendues
		\item systèmes de fichiers multiples
		\item protocoles réseau multiples
		\item ...
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Noyau monolithique -- Développement noyau}
    Développer le noyau (mode superviseur) est difficile :
    \begin {itemize}
	\item pas d'outils de mise au point
	    \begin {itemize}
		\item débogueur source, mode \frquote{pas à pas}, etc.
	    \end {itemize}
	\item pas de bibliothèque à disposition
	    \begin {itemize}
		\item pas de \code{printf}/\code{strcpy}/etc. : il faut
		    les écrire soi-même

	    \end {itemize}
	\item interactions avec le matériel parfois difficiles
	    \begin {itemize}
		\item pas ou peu de documentation
		\item comportement non conforme aux spécifications
		    (si elles existent)

	    \end {itemize}
	\item bug \implique \emph{crash}/\emph{freeze} du noyau
	    \begin {itemize}
		\item pas de diagnostic \implique où est le bug ?
		\item reboot \implique lent
		\item corruption du disque \implique réinstaller
	    \end {itemize}
    \end {itemize}

    \medskip

    Bémol~: avec la virtualisation et \code{qemu}, le développement
    est facilité
    \begin {itemize}
	\item utilisation possible de \code{gdb} avec \code{qemu}
	\item cf. UE conception de systèmes d'exploitation en master SIRIS

    \end {itemize}
\end {frame}

\begin {frame} {Noyau monolithique -- Limites et avantages}
    Limites des systèmes monolithiques :
    \begin {itemize}
	\item développement plus complexe
	\item pas très extensible
	\item un gros programme contient plus de bugs qu'un petit
	\item pas de protection entre les différentes parties du
	    noyau
	    \begin {itemize}
		\item un bug dans un obscur sous-système du noyau peut
		    affecter le système entier :
		    \begin {itemize}
			\item \emph{crash}
			\item compromission de la sécurité
		    \end {itemize}

	    \end {itemize}
    \end {itemize}

    \bigskip

    Mais avantages :
    \begin {itemize}
	\item développement bien compris et maîtrisé
	    \begin {itemize}
		\item la preuve : Linux avec plus de 20 millions de lignes
	    \end {itemize}
	\item bonnes performances
	    \begin {itemize}
		\item pas de \frquote{frontière} à franchir entre
		    les différents sous-systèmes

	    \end {itemize}
    \end {itemize}
\end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Extension de noyau
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Extension de noyau}

\begin {frame} {Extension de noyau}
    De nombreux systèmes (Linux, BSD, Windows, OS X) ont la possibilité
    de charger dynamiquement des extensions du noyau :

    \begin {itemize}
	\item FreeBSD : commande \code{kldload}
	\item Linux : commande \code{modprobe}
    \end {itemize}

    \bigskip

    Une extension :
    \begin {itemize}
	\item est un fichier \code{.o} compilé
	    \begin {itemize}
		\item certains symboles sont non résolus
		    \begin {itemize}
			\item exemple : variable \code{curproc}
			    (cf. Zorglub33)
			    \\
			    variable utilisée par l'extension, mais
			    non définie par l'extension

		    \end {itemize}

	    \end {itemize}

	\item contient 3 choses :
	    \begin {itemize}
		\item une fonction d'insertion
		\item une fonction de suppression
		\item les fonctions formant le cœur de l'extension
	    \end {itemize}

    \end {itemize}
\end {frame}

\begin {frame} {Extension de noyau}
    Chargement d'une extension : exemple de Linux (\code{insmod}) :

    \begin {itemize}
	\item \code{insmod} charge le fichier dans un buffer et appelle
	    la primitive système \code{init\_module} (spécifique Linux)

	\item la primitive s'exécute en mode privilégié :

	    \begin {itemize}
		\item trouve des pages libres pour charger l'extension
		\item charge l'extension depuis le buffer
		\item résout les symboles non résolus
		\item appelle la fonction d'insertion de l'extension
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Extension de noyau}
    Au déchargement d'une extension : exemple de Linux (\code{rmmod}) :

    \begin {itemize}
	\item \code{rmmod} appelle la primitive système
	    \code{delete\_module} (spécifique Linux)

	\item la primitive s'exécute en mode privilégié :

	    \begin {itemize}
		\item vérifie qu'il n'y a pas actuellement d'utilisation
		    du module

		\item appelle la fonction de suppression de l'extension
		\item libère la mémoire utilisée par l'extension
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Extension de noyau}
    Pour être utiles, les fonctions formant le cœur de l'extension
    doivent être appelées :

    \begin {itemize}
	\item la fonction d'insertion met éventuellement à jour
	    les tables nécessaires pour appeler l'extension

	    \begin {itemize}
		\item nouvelle primitive : utiliser une entrée libre
		    dans le tableau des primitives (cf. chapitre
		    \frquote{mécanismes de base}, tableau
		    \code{tabprim} sur Zorglub33)

		\item nouveau pilote de périphérique : utiliser une
		    entrée libre dans le tableau des pilotes (cf.
		    chapitre \frquote{pilotes de périphériques},
		    tableau \code{cdevsw} sur Zorglub33)

		\item etc.

	    \end {itemize}

	\item l'appel aux fonctions de l'extension doit donc être prévu
	    dans le noyau initial :
	    le noyau doit prévoir des \emph{hooks}
	    \begin {itemize}
		\item les entrées libres dans les tables (table
		    des primitives, tables de pilotes, table des systèmes
		    de fichiers, etc.) sont des \emph{hooks}

	    \end {itemize}

    \end {itemize}
\end {frame}

\begin {frame} {Extension de noyau}
    \fig {kmod}{}
\end {frame}

\begin {frame} {Extension de noyau}
    Avantages :
    \begin {itemize}
	\item étend effectivement le noyau
	\item économie de mémoire
	    \begin {itemize}
		\item si le noyau est découpé en extensions, il n'y
		    a pas besoin de tout charger en mémoire
		    \begin {itemize}
			\item ex : \code{lsmod | wc -l} donne 171
			    extensions chargées (sur \numprint{5355})
		    \end {itemize}
	    \end {itemize}
	\item flexible 
	\item pas de pénalités de performances
	\item oblige à définir des interfaces entre le noyau et les
	    extensions
    \end {itemize}
    
    \bigskip

    Inconvénients :
    \begin {itemize}
	\item extension limitée à ce qui est prévu dans le noyau
	\item pas plus de protection qu'un noyau monolithique
	    \begin {itemize}
		\item une extension boguée peut planter tout le noyau
	    \end {itemize}
	\item problème de sécurité si extension non sûre
	    \begin {itemize}
		\item attention à la provenance des extensions
	    \end {itemize}
    \end {itemize}
\end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Micronoyau
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Micronoyau}

\begin {frame} {Micronoyau -- Principes}
    Un noyau monolithique :
    \begin {itemize}
	\item implémente des abstractions de \frquote{haut niveau} :
	    \begin {itemize}
		\item utilisateurs, groupes, systèmes de fichiers,
		    pilotes de périphériques, tubes, politique
		    d'ordonnancement, etc.

	    \end {itemize}
    \end {itemize}
    \bigskip
    Principe des micronoyaux :
    \begin {itemize}
	\item \frquote{remonter} les abstractions de haut niveau en
	    mode utilisateur
	    \begin {itemize}
		\item sous forme de \frquote{services}
	    \end {itemize}
	\item mode superviseur : seulement les abstractions de
	    \frquote{bas niveau}

	    \begin {itemize}
		\item abstractions minimales pour l'implémentation
		    des services
		\item remonter tout ce qui est possible en mode utilisateur
		\item approche minimaliste pour le mode superviseur
	    \end {itemize}
	\item appels système : concernent les abstractions du micronoyau
	    \begin {itemize}
		\item micronoyau L4 : environ 20 primitives seulement
		\item API (type POSIX) classiques :
			implémentées par un service
		    \begin {itemize}
			\item invisible pour les applications : appel
			    au service masqué par la bibliothèque
			    standard

		    \end {itemize}
	    \end {itemize}

    \end {itemize}
\end {frame}

\begin {frame} {Micronoyau -- Principes}
    Exemples d'abstractions de bas niveau gérées en mode superviseur :
    \begin {itemize}
	\item gestion de la mémoire physique
	\item tâches et threads, ordonnancement basique
	\item mécanisme de communication entre processus
	    (IPC)
	    \begin {itemize}
		\item IPC (\emph{Inter-Process Communication\/})
		\item à base de passage de messages
	    \end {itemize}
    \end {itemize}
    \implique il est indispensable d'en optimiser l'implémentation

    \bigskip

    Exemples de services (implémentés en mode utilisateur) :
    \begin {itemize}
	\item systèmes de fichiers
	\item pilotes de périphériques
	\item API POSIX, API Win32
	\item gestion du swap et de la pagination
	\item etc.
    \end {itemize}

\end {frame}

\begin {frame} {Micronoyau -- Principes}
    \fig {microk} {}
\end {frame}

\begin {frame} {Micronoyau -- Historique}
    \begin {itemize}
	\item 1969 : RC 4000 Multiprogramming System (Brinch Hansen)
	\item 1981 : apparition du mot \frquote{micronoyau}
	\item 1987 : Minix 1.0 (Andrew Tanenbaum)
	\item 1990 : Mach 3.0 (Carnegie Mellon University)
	    \begin {itemize}
		\item preuve de faisabilité
		\item pas assez \frquote{micro} (trop volumineux)
		\item performances médiocres
		\item à la base de OSF/1, XNU, MacOS X, GNU Hurd, etc.
	    \end {itemize}
	\item milieu des années 1990 : L4 (Jochen Liedtke)
	    \begin {itemize}
		\item implémentation optimisée
		\item regain d'intérêt pour les micronoyaux
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Micronoyau}
    Avantages :
    \begin {itemize}
	\item protection des services
	    \begin {itemize}
		\item un service peut crasher sans affecter les autres
		\item meilleure stabilité

	    \end {itemize}

	\item développement facilité
	    \begin {itemize}
		\item services : utiliser les facilités du mode
		    utilisateur (débogueur, etc.)
		\item micronoyau : plus simple
	    \end {itemize}

	\item flexibilité : services adaptables pour un environnement spécifique
	\item adapté aux systèmes distribués : un service peut être distant
    \end {itemize}

    \bigskip

    Inconvénients :
    \begin {itemize}
	\item performances
	    \begin {itemize}
		\item grand nombre d'appels système
		\item mécanisme de passage de messages
	    \end {itemize}
	    \implique d'où l'importance de l'optimisation des mécanismes

    \end {itemize}

\end {frame}

\begin {frame} {Micronoyau -- Système hybride}

    Systèmes hybrides :

    \begin {itemize}
	\item réintégration de certains services en mode superviseur
	    \begin {itemize}
		\item améliorer les performances
		\item limiter la traversée de frontière entre mode
		    utilisateur et mode superviseur
	    \end {itemize}
	\item systèmes commerciaux : MacOS X, Windows
    \end {itemize}

\end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Exokernel et Unikernel
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Autres approches}

\begin {frame} {Exokernel et Unikernel}
    D'autres approches ont été proposées pour simplifier l'architecture
    des systèmes :

    \begin {itemize}
	\item Exokernel et LibOS
	\item Unikernel
    \end {itemize}

    \medskip

    \implique encore du domaine de la recherche \\
    \implique prototypes réalisés
\end {frame}

\begin {frame} {Exokernel}
    Idée :
    \begin {itemize}
	\item réduire au maximum le noyau
	    \begin {itemize}
		\item encore plus qu'avec un micronoyau
	    \end {itemize}
	\item donner aux applications l'accès quasi direct au matériel
	\item séparer la protection de la gestion
    \end {itemize}

    \medskip

    Accès au matériel :
    \begin {itemize}
	\item à chaque \alert{tâche} est allouée une partie de la machine
	    \begin {itemize}
		\item une portion du temps processeur
		\item des pages en mémoire physique
		\item des périphériques
		\item des portions du disque
		\item routage des interruptions matérielles
	    \end {itemize}
	\item l'exokernel ne fait que contrôler l'accès à ces ressources
	\item chaque tâche est une sorte de machine virtuelle
    \end {itemize}
\end {frame}

\begin {frame} {Exokernel}
    Chaque tâche s'exécute en mode utilisateur :
    \begin {itemize}
	\item abstractions de haut niveau
	    \begin {itemize}
		\item fournies par chaque tâche
		\item implémentation sous forme d'une bibliothèque
		    de fonctions
	    \end {itemize}

	\item concept de \frquote{Library Operating System} :
	    \begin {itemize}
		\item l'accès aux abstractions est implémenté sous
		    forme de fonctions de bibliothèque
		    \begin {itemize}
			\item systèmes de fichiers : \code{open},
			    \code{read}, etc.
			\item processus : \code{fork}, \code{wait}, etc.
			\item etc.
		    \end {itemize}
	    \end {itemize}
    \end {itemize}

    \medskip

    Prototypes réalisés :
    \begin {itemize}
	\item MIT : exokernel Aegis + libOS ExOS (1995)
	\item U. Cambridge : Nemesis
	\item etc.
    \end {itemize}

\end {frame}

\begin {frame} {Unikernel}
    Unikernel : approche inverse des exokernels
    \begin {itemize}
	\item exokernels : \alert{sortir} le \alert{maximum}
	    de choses du noyau

	\item unikernels : \alert{rentrer} le \alert{minimum}
	    de choses dans le noyau

    \end {itemize}

    \medskip

    Principe des unikernels :
    \begin {itemize}
	\item spécialiser pour mieux virtualiser
	    \begin {itemize}
		\item environnements de type \frquote{serveur virtualisé}
		    avec une application par serveur virtuel
		\item spécialiser le noyau pour cette application
		\item retirer le maximum d'éléments inutiles du noyau
	    \end {itemize}
	\item décomposer le noyau en éléments indépendants
	    \begin {itemize}
		\item utilisation des concepts de libOS
		\item ne prendre que les briques nécessaires
	    \end {itemize}
	\item l'application est elle-même embarquée dans le noyau
	\item bonnes performances
    \end {itemize}
\end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Hommage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% \titreB {Hommage}

\begin {frame} {Hommage}
    Hommage à André Franquin (1924--1997)

    % \bigskip

    \hfill
    \fig {franquin-zorglub} {.3}

    \bigskip

    {\fC
    \url {http://www.franquin.com/spirou_fantasio/perso_zorglub_spirou.php}
    }
\end {frame}
