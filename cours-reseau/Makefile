#
# Cibles possibles :
# all: tous les chapitres individuels (ch-lan.pdf, ch-ipv4.pdf, etc.)
# ch-lan.pdf: un chapitre individuel particulier
# ch-ipv4.pdf: idem
# ...
# tout.pdf: un document contenant tous les chapitres (pas fait avec "all")
# print: tous les PDF en version 3 slides par page
#

.SUFFIXES:	.pdf .fig .svg .tex

.fig.pdf:
	fig2dev -L pdf $*.fig $*.pdf

.svg.pdf:
	inkscape --export-filename=$@ $<

.tex.pdf:
	pdflatex $*
	pdflatex $*

# pour la cible print
PRINTCMD = pdfjam --quiet --paper a4paper --keepinfo \
	--nup 2x3 --frame true --delta "0.2cm 0.2cm" --scale 0.95

DEPS	= courspda.sty term.pdf logo-uds.pdf \
	term.pdf \
	annee.tex \
	licence.tex by-nc.pdf

##############################################################################
# Histoire des réseaux

SRChist = ch-hist.tex sl-hist.tex

FIGhist = \
	inc-hist/telegraphe.pdf \
	inc-hist/morse-code.pdf \
	inc-hist/modem.pdf \
	inc-hist/multiplex.pdf \
	inc-hist/topo.pdf \
	inc-hist/couches-iso-1.pdf \
	inc-hist/encap.pdf \
	inc-hist/gw.pdf \
	inc-hist/collision.pdf \
	inc-hist/adr-eth.pdf \
	inc-hist/switch-1.pdf \
	inc-hist/switch.pdf \
	inc-hist/comm-pkt.pdf \
	inc-hist/couches-iso-2.pdf \
	inc-hist/arpanet-1.pdf \
	inc-hist/arpanet-2.pdf \
	inc-hist/arpa1969.pdf \
	inc-hist/arpa-croiss.pdf \
	inc-hist/adr-ip.pdf \
	inc-hist/ip-fiab-1.pdf \
	inc-hist/ip-fiab-2.pdf \
	inc-hist/nsfnet-1.pdf \
	inc-hist/nsfnet-2.pdf \
	inc-hist/nsfnet-3.pdf \
	inc-hist/priv-nsf.pdf \
	inc-hist/noc.pdf \
	inc-hist/gouv.pdf \
	inc-hist/nbhosts.pdf \

IMGhist = \
	inc-hist/relais-poste.jpg \
	inc-hist/chappe-fr.png \
	inc-hist/chappe-haut-barr.jpg \
	inc-hist/morse-manip.jpg \
	inc-hist/morse-recv.jpg \
	inc-hist/cable-vieux.jpg \
	inc-hist/cable-actuel.png \
	inc-hist/uss-niagara.jpg \
	inc-hist/marconi.jpg \
	inc-hist/am-fm.png \
	inc-hist/baudot-code.png \
	inc-hist/baudot-clavier.jpg \
	inc-hist/baudot-papier.jpg \
	inc-hist/telex-siemens.jpg \
	inc-hist/telex-courbe.png \
	inc-hist/sputnik1.jpg \
	inc-hist/metcalfe-ethernet.png \
	inc-hist/eth-10base5.jpg \
	inc-hist/eth-10base2.png \
	inc-hist/eth-10baset.png \
	inc-hist/eth-100basefx-lc.jpg \
	inc-hist/netgear-gs105.jpg \
	inc-hist/juniper-mx960.jpg \
	inc-hist/minitel.jpg \
	inc-hist/cyclades.png \
	inc-hist/kahn.jpg \
	inc-hist/cerf.jpg \
	inc-hist/demo1977.jpg \
	inc-hist/mosaic.jpg \
	inc-hist/pigeon.jpg \
	inc-hist/nbusers.png \
	inc-hist/continent.png \

##############################################################################
# Reseaux locaux

SRClan = ch-lan.tex sl-lan.tex

FIGlan = \
	inc-lan/aloha.pdf \
	inc-lan/mac48.pdf \
	inc-lan/80211-adr3.pdf \
	inc-lan/80211-assoc.pdf \
	inc-lan/80211-chan2.pdf \
	inc-lan/80211-chan5.pdf \
	inc-lan/80211-ctl.pdf \
	inc-lan/80211-fmt.pdf \
	inc-lan/80211-prob.pdf \
	inc-lan/8021q.pdf \
	inc-lan/8023ad.pdf \
	inc-lan/codage.pdf \
	inc-lan/10baset.pdf \
	inc-lan/csma-ca.pdf \
	inc-lan/csma.pdf \
	inc-lan/csmacd-delai.pdf \
	inc-lan/eth-fmt-eth2.pdf \
	inc-lan/eth-fmt-8023.pdf \
	inc-lan/commut.pdf \
	inc-lan/portsteal.pdf \
	inc-lan/sta-cachee.pdf \
	inc-lan/sta-exposee.pdf \
	inc-lan/stp-auto.pdf \
	inc-lan/stp-topo.pdf \
	inc-lan/stp.pdf \
	inc-lan/vlan-prc.pdf \

##############################################################################
# IPv4

SRCipv4 = ch-ipv4.tex sl-ipv4.tex

FIGipv4 = \
	inc-ipv4/arp-pb.pdf \
	inc-ipv4/arp-1.pdf \
	inc-ipv4/arp-2.pdf \
	inc-ipv4/arp-fmt.pdf \
	inc-ipv4/arp-pois.pdf \
	inc-ipv4/cidr-princ.pdf \
	inc-ipv4/cidr-route.pdf \
	inc-ipv4/gw.pdf \
	inc-ipv4/icmp-fmt.pdf \
	inc-ipv4/icmp-red.pdf \
	inc-ipv4/icmp-sq.pdf \
	inc-ipv4/ip-fmt.pdf \
	inc-ipv4/ip-frag1.pdf \
	inc-ipv4/ip-frag2.pdf \
	inc-ipv4/ip-ttl.pdf \
	inc-ipv4/ip.pdf \
	inc-ipv4/rt-def.pdf \
	inc-ipv4/rt-gen.pdf \
	inc-ipv4/rt-loc.pdf \
	inc-ipv4/rt-msg.pdf \
	inc-ipv4/sub-adr.pdf \
	inc-ipv4/sub-prc.pdf \
	inc-ipv4/sub-route.pdf \

LSTipv4	= \
	inc-ipv4/arp.sh \
	inc-ipv4/algo-route.c \
	inc-ipv4/algo-subnet.c \
	inc-ipv4/algo-rtsub.c \
	inc-ipv4/ping.sh \
	inc-ipv4/traceroute.sh \

##############################################################################
# Multicast

SRCmcast = ch-mcast.tex sl-mcast.tex

FIGmcast = \
	inc-mcast/princ.pdf \
	inc-mcast/eth-bit.pdf \
	inc-mcast/eth-encap.pdf \
	inc-mcast/igmp-fmt.pdf \
	inc-mcast/igmp-query.pdf \
	inc-mcast/igmp-join.pdf \
	inc-mcast/igmp-leave.pdf \
	inc-mcast/algo-flood.pdf \
	inc-mcast/algo-span.pdf \
	inc-mcast/algo-rpb1.pdf \
	inc-mcast/algo-rpb2.pdf \
	inc-mcast/algo-rpm.pdf \
	inc-mcast/pim-recv.pdf \
	inc-mcast/pim-send.pdf \
	inc-mcast/pim-chg.pdf \
	inc-mcast/mbone-pres.pdf \
	inc-mcast/mbone-encap.pdf \
	inc-mcast/mbone-fr.pdf \
	inc-mcast/couches.pdf \
	inc-mcast/sap-fmt.pdf \
	inc-mcast/rtp-trad.pdf \
	inc-mcast/rtp-mix.pdf \
	inc-mcast/rtp-fmt.pdf \
	inc-mcast/rtp-pb.pdf \
	inc-mcast/rsvp-princ.pdf \
	inc-mcast/rsvp-fmt.pdf \
	inc-mcast/rsvp-depl.pdf \

##############################################################################
# IPv6

SRCipv6 = ch-ipv6.tex sl-ipv6.tex

FIGipv6 = \
	inc-ipv6/alloc-ipv4.pdf \
	inc-ipv6/alloc-rir.pdf \
	inc-ipv6/adr-7217.pdf \
	inc-ipv6/adr-any.pdf \
	inc-ipv6/adr-eui64.pdf \
	inc-ipv6/adr-if48.pdf \
	inc-ipv6/adr-loc1.pdf \
	inc-ipv6/adr-loc2.pdf \
	inc-ipv6/adr-mul.pdf \
	inc-ipv6/adr-uni1.pdf \
	inc-ipv6/adr-uni2.pdf \
	inc-ipv6/auto.pdf \
	inc-ipv6/dns-icmp.pdf \
	inc-ipv6/flow.pdf \
	inc-ipv6/frag.pdf \
	inc-ipv6/hdr-auth.pdf \
	inc-ipv6/hdr-chain.pdf \
	inc-ipv6/hdr-frag.pdf \
	inc-ipv6/hdr-icmp.pdf \
	inc-ipv6/hdr-jumbo.pdf \
	inc-ipv6/hdr-opt.pdf \
	inc-ipv6/hdr-rt.pdf \
	inc-ipv6/hdr-rt0.pdf \
	inc-ipv6/hdr-tlv.pdf \
	inc-ipv6/hdr-v6.pdf \
	inc-ipv6/hdrs.pdf \
	inc-ipv6/tran-4444.pdf \
	inc-ipv6/tran-4t66.pdf \
	inc-ipv6/tran-6444.pdf \
	inc-ipv6/tran-6446.pdf \
	inc-ipv6/tran-6646.pdf \
	inc-ipv6/tran-6666.pdf \
	inc-ipv6/tunnel1.pdf \
	inc-ipv6/tunnel2.pdf \

##############################################################################
# Transport

SRCxport = ch-xport.tex sl-xport.tex

FIGxport = \
	inc-xport/ip-spooftcp.pdf \
	inc-xport/tcp-auto.pdf \
	inc-xport/tcp-con.pdf \
	inc-xport/tcp-fast.pdf \
	inc-xport/tcp-fin.pdf \
	inc-xport/tcp-fmt.pdf \
	inc-xport/tcp-hijack.pdf \
	inc-xport/tcp-nagle.pdf \
	inc-xport/tcp-optex.pdf \
	inc-xport/tcp-optfmt.pdf \
	inc-xport/tcp-seq.pdf \
	inc-xport/tcp-syncookie.pdf \
	inc-xport/tcp-synflood.pdf \
	inc-xport/tcp-win.pdf \
	inc-xport/tcp-winack.pdf \
	inc-xport/tcp-ts.pdf \
	inc-xport/udp-fmt.pdf \
	inc-xport/nat-simple.pdf \
	inc-xport/nat-pat.pdf \

LSTxport = \
	inc-xport/acl.sh \

##############################################################################
# DNS

SRCdns = ch-dns.tex sl-dns.tex

FIGdns = \
	inc-dns/dns-fmt.pdf \
	inc-dns/dns-fmtq.pdf \
	inc-dns/dns-fmtrr.pdf \
	inc-dns/tree-1.pdf \
	inc-dns/tree-2.pdf \
	inc-dns/replic.pdf \
	inc-dns/req.pdf \
	inc-dns/mandat.pdf \
	inc-dns/arpa.pdf \
	inc-dns/poison.pdf \
	inc-dns/vuln.pdf \
	inc-dns/rrsig.pdf \
	inc-dns/ds.pdf \
	inc-dns/vues.pdf \

LSTdns = \
	inc-dns/nslookup.sh \
	inc-dns/dig.sh \
	inc-dns/toys.sh \

##############################################################################
# Programmation

SRCprog = ch-prog.tex sl-prog.tex

FIGprog = \
	inc-prog/fct-prc.pdf \
	inc-prog/getai-cli.pdf \
	inc-prog/getai-srv.pdf \
	inc-prog/sock-prc.pdf \
	inc-prog/sock-tcp.pdf \
	inc-prog/sock-udp.pdf \
	inc-prog/sockaddr.pdf \
	inc-prog/syslog.pdf \

LSTprog	= \
	inc-prog/addrinfo.h \
	inc-prog/getai-cli.c \
	inc-prog/getai-srv.c \
	inc-prog/protoent.h \
	inc-prog/servent.h \
	inc-prog/sockaddr.h \
	inc-prog/sockaddr_in.h \
	inc-prog/sockaddr_in6.h \
	inc-prog/sockaddr_un.h \
	inc-prog/tcpcli4.c \
	inc-prog/tcpserv4.c \
	inc-prog/udpcli4.c \
	inc-prog/udpserv4.c \
	inc-prog/demon.c \
	inc-prog/inetd.c \

##############################################################################
# TLS

SRCtls = ch-tls.tex sl-tls.tex

FIGtls = \
	inc-tls/ssl-pile.pdf \
	inc-tls/ssl-pro0.pdf \
	inc-tls/ssl-pro1.pdf \
	inc-tls/ssl-pro2.pdf \
	inc-tls/ssl-secret.pdf \

##############################################################################
# Applications

SRCapplis = ch-applis.tex sl-applis.tex

FIGapplis = \
	inc-applis/tftp-pro.pdf \
	inc-applis/tftp-xmp.pdf \
	inc-ipv4/arp-2.pdf \
	inc-applis/bootp-fmt.pdf \
	inc-applis/bootp-rly.pdf \
	inc-applis/dhcp-auto.pdf \
	inc-applis/dhcp-fmt.pdf \
	inc-applis/dsk-prc.pdf \
	inc-applis/dsk-sun.pdf \
	inc-applis/ftp-con.pdf \
	inc-applis/ftp-pasv.pdf \
	inc-applis/ftp-pro.pdf \
	inc-applis/nfs-ino.pdf \
	inc-applis/nfs-nfs.pdf \
	inc-applis/nfs-ufs.pdf \
	inc-applis/nfs-vno.pdf \
	inc-applis/rpc-pmap.pdf \
	inc-applis/rpc-rpcgen.pdf \
	inc-applis/rpc-xdr.pdf \
	inc-applis/rsh.pdf \
	inc-applis/mail-arch.pdf \
	inc-applis/mail-msg.pdf \
	inc-applis/smtp-msg.pdf \
	inc-applis/smtp-pro.pdf \
	inc-applis/smtp-codage.pdf \
	inc-applis/mime-multi.pdf \
	inc-applis/mime-exmulti.pdf \
	inc-applis/ssh-archi.pdf \
	inc-applis/ssh-chan.pdf \
	inc-applis/ssh-pro0.pdf \
	inc-applis/ssh-pro1.pdf \
	inc-applis/ssh-pro2.pdf \
	inc-applis/ssh-pro3.pdf \
	inc-applis/ssh-pubkey.pdf \
	inc-applis/teln-opt.pdf \
	inc-applis/teln-prc.pdf \
	inc-applis/teln-subopt.pdf \
	inc-applis/x11-pro.pdf \
	inc-applis/x11-term.pdf \

LSTapplis = \
	inc-applis/ftp.sh \

##############################################################################
# Routage

SRCroute = ch-route.tex sl-route.tex

FIGroute = \
	inc-route/rip-xmp.pdf \
	inc-route/rip-fmt.pdf \


##############################################################################
# L'ensemble

SRCall = \
	$(SRClan) \
	$(SRCipv4) \
	$(SRCmcast) \
	$(SRCipv6) \
	$(SRCxport) \
	$(SRCdns) \
	$(SRCprog) \
	$(SRCtls) \
	$(SRCapplis) \
	$(SRCroute) \
	tout.tex

FIGall = \
	$(FIGhist) \
	$(FIGlan) \
	$(FIGipv4) \
	$(FIGmcast) \
	$(FIGipv6) \
	$(FIGxport) \
	$(FIGdns) \
	$(FIGprog) \
	$(FIGtls) \
	$(FIGapplis) \
	$(FIGroute) \

IMGall = \
	$(IMGhist) \
	$(IMGlan) \
	$(IMGipv4) \
	$(IMGmcast) \
	$(IMGipv6) \
	$(IMGxport) \
	$(IMGdns) \
	$(IMGprog) \
	$(IMGtls) \
	$(IMGapplis) \
	$(IMGroute) \

LSTall = \
	$(LSThist) \
	$(LSTlan) \
	$(LSTipv4) \
	$(LSTmcast) \
	$(LSTipv6) \
	$(LSTxport) \
	$(LSTdns) \
	$(LSTprog) \
	$(LSTtls) \
	$(LSTapplis) \
	$(LSTroute) \

##############################################################################
# Les cibles
##############################################################################

all:	ch-hist.pdf ch-lan.pdf ch-ipv4.pdf ch-mcast.pdf ch-xport.pdf ch-ipv6.pdf
all:	ch-dns.pdf ch-prog.pdf ch-tls.pdf ch-applis.pdf ch-route.pdf

ch-hist.pdf:	$(DEPS) $(FIGhist)   $(IMGhist)   $(LSThist)   $(SRChist)
ch-lan.pdf:	$(DEPS) $(FIGlan)    $(IMGlan)    $(LSTlan)    $(SRClan)
ch-ipv4.pdf:	$(DEPS) $(FIGipv4)   $(IMGipv4)   $(LSTipv4)   $(SRCipv4)
ch-mcast.pdf:	$(DEPS) $(FIGmcast)  $(IMGmcast)  $(LSTmcast)  $(SRCmcast)
ch-ipv6.pdf:	$(DEPS) $(FIGipv6)   $(IMGipv6)   $(LSTipv6)   $(SRCipv6)
ch-xport.pdf:	$(DEPS) $(FIGxport)  $(IMGxport)  $(LSTxport)  $(SRCxport)
ch-dns.pdf:	$(DEPS) $(FIGdns)    $(IMGdns)    $(LSTdns)    $(SRCdns)
ch-prog.pdf:	$(DEPS) $(FIGprog)   $(IMGprog)   $(LSTprog)   $(SRCprog)
ch-tls.pdf:	$(DEPS) $(FIGtls)    $(IMGtls)    $(LSTtls)    $(SRCtls)
ch-applis.pdf:	$(DEPS) $(FIGapplis) $(IMGapplis) $(LSTapplis) $(SRCapplis)
ch-route.pdf:	$(DEPS) $(FIGroute)  $(IMGroute)  $(LSTroute)  $(SRCroute)

inc-hist/switch-1.pdf: inc-hist/switch.fig
	fig2dev -L pdf -D +1:30,40:99 -K $< $@
inc-hist/couches-iso-1.pdf: inc-hist/couches-iso.fig
	fig2dev -L pdf -D +50:60 -K $< $@
inc-hist/couches-iso-2.pdf: inc-hist/couches-iso.fig
	fig2dev -L pdf -D +25:59 -K $< $@
inc-hist/ip-fiab-1.pdf: inc-hist/ip-fiab.fig	# la totale
	fig2dev -L pdf -D +1:99 -K $< $@
inc-hist/ip-fiab-2.pdf: inc-hist/ip-fiab.fig	# seules les couches IP
	fig2dev -L pdf -D +60:61 -K $< $@
inc-hist/arpanet-1.pdf: inc-hist/arpanet.fig
	fig2dev -L pdf -D +10,19:99 -K $< $@
inc-hist/arpanet-2.pdf: inc-hist/arpanet.fig
	fig2dev -L pdf -D +1:9,19:99 -K $< $@
inc-hist/arpa-croiss.pdf: inc-hist/arpa-croiss.fig \
			inc-hist/arpa196912.jpg inc-hist/arpa197006.jpg \
			inc-hist/arpa197012.jpg inc-hist/arpa197109.jpg \
			inc-hist/arpa197208.jpg inc-hist/arpa197507.jpg
inc-hist/nsfnet-1.pdf: inc-hist/nsfnet.fig
	fig2dev -L pdf -D +58:99 -K $< $@
inc-hist/nsfnet-2.pdf: inc-hist/nsfnet.fig
	fig2dev -L pdf -D +50:99 -K $< $@
inc-hist/nsfnet-3.pdf: inc-hist/nsfnet.fig
	fig2dev -L pdf -D +20:22,59:99 -K $< $@
inc-hist/noc.pdf: inc-hist/noc.fig inc-hist/noc-att.png

inc-ipv4/arp-1.pdf: inc-ipv4/arp.fig		# arp
	fig2dev -L pdf -D +1:20,40:99 -K $< $@
inc-ipv4/arp-2.pdf: inc-ipv4/arp.fig		# rarp
	fig2dev -L pdf -D +1:30,50:99 -K $< $@

inc-dns/tree-1.pdf: inc-dns/tree.fig
	fig2dev -L pdf -D +50 -K $< $@
inc-dns/tree-2.pdf: inc-dns/tree.fig
	fig2dev -L pdf -D +50:52 -K $< $@

tout.pdf:	$(DEPS) $(FIGall) $(LSTall) $(SRCall)

print-tout.pdf: tout.pdf
	$(PRINTCMD) -o print-tout.pdf tout.pdf

print:	all print-tout.pdf
	for i in ch-*.pdf ; do \
	    $(PRINTCMD) -o print-$$i $$i ; \
	done

clean:
	rm -f $(FIGall)
	rm -f *.bak */*.bak *.nav *.out *.snm *.vrb *.log *.toc *.aux
	rm -f print-*.pdf ch*.pdf tout*.pdf by-nc.pdf
	rm -f casserole.pdf term.pdf penseur.pdf
	rm -f icone-*.png
	rm -f inc?-?-*/a.out
