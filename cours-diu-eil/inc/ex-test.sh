#!/bin/sh

N=1
while [ "$#" -ge 1 ]
do
    if [ -f "$1" ]
    then
       echo "$N: $1 est un fichier"
    elif [ -d "$1" ]
    then
       echo "$N: $1 est un repertoire"
    else
       echo "$N: $1 est autre chose"
    fi
    N=$((N+1))
    shift
done
exit 0
