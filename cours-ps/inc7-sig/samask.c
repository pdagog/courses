void recevoir_sigint (int sig) {	// SIGHUP est automatiquement masqué
    ajouter_dans_struct_donnees () ;	// ne doit pas être interrompu par \code{retirer\_de\_struct\_donnees}
}					// SIGHUP est automatiquement démasqué

void recevoir_sighup (int sig) {	// SIGINT est automatiquement masqué
    retirer_de_struct_donnees () ;	// ne doit pas être interrompu par \code{ajouter\_dans\_struct\_donnees}
}					// SIGINT est automatiquement démasqué

int main (...) {
    struct sigaction s1, s2 ;

    s1.sa_handler = recevoir_sigint ;
    s1.sa_flags = 0 ;
    sigemptyset (&s1.sa_mask) ;
    sigaddset (&s1.sa_mask, SIGHUP) ;	// masquer SIGHUP pendant l'exécution de la fonction
    sigaction (SIGINT, &s1, NULL) ;

    s2.sa_handler = recevoir_sighup ;
    s2.sa_flags = 0 ;
    s2gemptyset (&s2.sa_mask) ;
    s2gaddset (&s2.sa_mask, SIGINT) ;	// masquer SIGINT pendant l'exécution de la fonction
    s2gaction (SIGHUP, &s2, NULL) ;

    ...
}
