sigset_t new, old, vide ;

sigemptyset (&new) ;
sigaddset (&new, SIGINT) ;
sigprocmask (SIG_BLOCK, &new, &old) ;	// section critique : masquer SIGINT

if (! signal_recu) {
  sigemptyset (&vide) ;
  sigsuspend (&vide) ; 	 	  	// section critique levée pendant l'attente
}

sigprocmask (SIG_SETMASK, &old, NULL) ;	// fin de section critique : démasquer SIGINT

// faire ce qui doit être fait lorsque le signal est pris en compte
