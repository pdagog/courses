struct sigaction {
  void    (*sa_handler) (int) ; // adresse d'une fonction
  sigset_t  sa_mask ;           // ensemble de signaux à masquer
  int       sa_flags ;          // pour modifier le comportement
  ...                           // d'autres champs existent
} ;
