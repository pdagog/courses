# Date affichée par défaut : dernière modification des données
> ls -l ch1-intro.tex
-rw-r--r-- 1 pda users 378 Jan 22  2019 ch1-intro.tex

# Date affichée avec \texttt{-c} : dernière modification des attributs
> ls -lc ch1-intro.tex
-rw-r--r-- 1 pda users 378 Jan  9 19:20 ch1-intro.tex

# Date affichée avec \texttt{-u} : dernier accès au fichier (= dernière \textbf{u}tilisation)
> ls -lu ch1-intro.tex
-rw-r--r-- 1 pda users 378 Jul  8 18:37 ch1-intro.tex
